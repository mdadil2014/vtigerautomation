package com.evs.vtiger.framework.generic_functions.dataUtil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.evs.vtiger.framework.generic_functions.PropUtil;

public class XLUtil implements ITestData {
	


	@Override
	public Map<String, String> getTestData(String tcID) throws IOException {
		// TODO Auto-generated method stub
		///  xlsx , xls
		///  XSSFWorkbook - xlsx
		///  HSSFWorkbook - xls
		Workbook wbook=getWorkbook();
		Sheet sheet=wbook.getSheet("data");
		int rowCount=sheet.getLastRowNum();
		Map<String, String> DataMap=new HashMap<String, String>();
		for(int i=1; i<=rowCount; i++){
			Row rowObj=sheet.getRow(i);
			String xlTCId=rowObj.getCell(2).getStringCellValue();
			if(tcID.equalsIgnoreCase(xlTCId)){
				int cellCount=rowObj.getLastCellNum();
				for(int j=3; j<=cellCount-1;j++){
					Cell cellObj=rowObj.getCell(j, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
					String keyName=getCellDataInStringFormat(cellObj);
					cellObj=rowObj.getCell(j+1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
					String keyValue=getCellDataInStringFormat(cellObj);
					
					DataMap.put(keyName, keyValue);
				}
				break;
			}

		}
     return DataMap;
	}

	public static String getCellDataInStringFormat(Cell cellObj){
		int cellTypeNumer=cellObj.getCellType();
		String data=null;
		if(cellTypeNumer==1){
			data=cellObj.getStringCellValue();
		}else if(cellTypeNumer==0){
			Double dblCellValue=cellObj.getNumericCellValue();
			Integer intCellValue=dblCellValue.intValue();
			data=intCellValue.toString();
		}
		return data;
	}
	
	
	
	public  Workbook getWorkbook() throws IOException{
		String path=PropUtil.getPropertyValue("testData.path");
		FileInputStream fis=new FileInputStream(path);
		Workbook wbook=null;
		if(path.contains("xlsx")){
			wbook=new XSSFWorkbook(fis);
		}else{
			wbook=new HSSFWorkbook(fis);

		}
		return wbook;
	}


}
