package com.evs.vtiger.framework.generic_functions.dataUtil;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.evs.vtiger.framework.generic_functions.PropUtil;

public class DataManager {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public  Map<String, String> TestDataMap;
	private static DataManager DM;
	private DataManager(){
		
	}
	public static DataManager getDataSource(){
		 if(DM==null){
			 DM = new DataManager();
		 }
		 return DM;
	}
	
	public  void getDataSourceType(String tcID) throws IOException {
		// TODO Auto-generated method stub
		String dataSource=PropUtil.getPropertyValue("testdata.source");
		ITestData dataObject=null;
		if(dataSource.equalsIgnoreCase("xl")){
			dataObject = new XLUtil();
		}else if(dataSource.equalsIgnoreCase("csv")){
			dataObject = new CSVUtil();
		}
		TestDataMap= dataObject.getTestData(tcID);	
	}

}
