package com.evs.vtiger.framework.generic_functions.dataUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ITestData {

	public Map<String, String> getTestData(String tcID)throws IOException;
	
	
}
