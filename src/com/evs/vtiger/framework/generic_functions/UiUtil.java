package com.evs.vtiger.framework.generic_functions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.evs.vtiger.framework.generic_functions.dataUtil.CSVUtil;
import com.evs.vtiger.framework.generic_functions.dataUtil.DataManager;
import com.evs.vtiger.framework.generic_functions.dataUtil.ITestData;
import com.evs.vtiger.framework.generic_functions.dataUtil.XLUtil;
import com.evs.vtiger.framework.reusable_pages.login.LoginPage;

public class UiUtil {

	/**
	 * @param args
	 */
	public static WebDriver driver;
	static Logger logger=Logger.getLogger("VTiger Execution Log");
	
	public static void loadLog4j() throws IOException{
		Properties prop=new Properties();
		FileInputStream fis=new FileInputStream("resources/config/log4J.properties");
	   prop.load(fis);
	   PropertyConfigurator.configure(prop);
	   logger.info("Test Execution Started......");
	}
	
	public static class Validations{
		
		public static void verifyInnerText(WebElement we, String validationName) throws IOException{
			Properties prop=new Properties();
			FileInputStream fis=new FileInputStream("resources/config/log4J.properties");
		   prop.load(fis);
		   PropertyConfigurator.configure(prop);
			String expValue= DataManager.getDataSource().TestDataMap.get(validationName);
			String actualText=we.getText();
			if(expValue.equalsIgnoreCase(actualText)){
				logger.fatal("");
			}else{
				
			}
		}



	}
	public static class Mouse{
		public static void hover(WebElement we){
			try{

				Actions act=new Actions(driver);
				act.moveToElement(we).build().perform();
				System.out.println("Clicked on WebElement");
			}catch(Exception e){
				System.out.println("element not clicked");
				e.printStackTrace();
			}

		}

		public static void click(WebElement we ){
			try{

				Actions act=new Actions(driver);
				act.click(we).build().perform();
				System.out.println("Clicked on WebElement");
			}catch(Exception e){
				System.out.println("element not clicked");
				e.printStackTrace();
			}

		}
	}
	public static class Wait{

		public static void waitForVisiblity(WebElement we, int timeOutInSeconds){
			WebDriverWait wt = new WebDriverWait(driver, timeOutInSeconds);
			wt.until(ExpectedConditions.visibilityOf(we));
		}
		public static void waitForVisiblity(By byObj, int timeOutInSeconds){
			WebDriverWait wt = new WebDriverWait(driver, timeOutInSeconds);
			wt.until(ExpectedConditions.visibilityOfElementLocated(byObj));
		}
		public static void waitForEnabling(){

		}



	}

	public static class JS{
		public static void click(WebElement we ){
			try{

				JavascriptExecutor js=(JavascriptExecutor)driver;
				js.executeScript("arguments[0].click", we);
				
			     logger.info("Clicked on WebElement");
			}catch(Exception e){
				
				
				logger.error("element not clicked");
			}

		}

		public static void input(WebElement we, String value ){
			try{

				JavascriptExecutor js=(JavascriptExecutor)driver;
				js.executeScript("arguments[0].value='"+value+"'", we);
				System.out.println("Clicked on WebElement");
			}catch(Exception e){
				System.out.println("element not clicked");
				e.printStackTrace();
			}

		}
	}

	public  static void launchBrowser() throws IOException{
		String browserName=PropUtil.getPropertyValue("browser.name");
		if(browserName.trim().equalsIgnoreCase("chrome")){
			System.setProperty("webdriver.chrome.driver", "resources/drivers/chromedriver.exe");
			driver= new ChromeDriver();
		}else if(browserName.trim().equalsIgnoreCase("firefox")){
			System.setProperty("webdriver.gecko.driver", "resources/drivers/geckodriver.exe");
			driver= new FirefoxDriver();
		}else if(browserName.trim().equalsIgnoreCase("ie")){
			System.setProperty("webdriver.ie.driver", "resources/drivers/IEDriverServer.exe");
			driver= new InternetExplorerDriver();
		}
		

	}

	public static LoginPage openLoginPage() throws IOException{
		launchBrowser();
		String url=PropUtil.getPropertyValue("application.url");
		openURL(url);
		LoginPage loginPage=PageFactory.initElements(UiUtil.driver, LoginPage.class);
      return loginPage;
		
	}
	
	public  static void openURL(String url){
		driver.get(url);

	}



	public static void click(WebElement we ){
		try{

			we.click();
			System.out.println("Clicked on WebElement");
		}catch(Exception e){
			System.out.println("element not clicked");
			e.printStackTrace();
		}

	}

	
	
	
//	public static String getDataFromList(String fieldName){
//		String value=null;
//		List<String> DL=DataManager.getDataSource().DataList;			
//           for(int i=0; i<=DL.size()-1; i++){
//				String data=DL.get(i);
//				if(data.equalsIgnoreCase(fieldName)){
//					value=DL.get(i+1);
//				}
//			}
//           return value;
//	}
	
	public static void input(WebElement we, String fieldName ){
		String value=DataManager.getDataSource().TestDataMap.get("fieldName");
		 ////String value=getDataFromList(fieldName);
		try{ 
            we.sendKeys(value);
			System.out.println(value+" is entered in test field");
		}catch(Exception e){
			System.out.println(value+" is note entered in test field");
			e.printStackTrace();
		}

	}

	public static void inputDirect(WebElement we, String value ){
		
		try{ 
           we.sendKeys(value);
			System.out.println(value+" is entered in test field");
		}catch(Exception e){
			System.out.println(value+" is note entered in test field");
			e.printStackTrace();
		}

	}
	
    public ITestData getDataSource(){
    	ITestData testData=new CSVUtil();
    	return testData;
    }
	
  public static void selectByText(WebElement we, String fieldName){
//	  String value=getDataFromList(fieldName);
//	  new Select(we).selectByVisibleText(value);
  }


}
