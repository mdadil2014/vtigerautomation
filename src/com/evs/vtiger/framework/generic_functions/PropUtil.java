package com.evs.vtiger.framework.generic_functions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropUtil {

	/**
	 * @param args
	 */
	public static Properties config;
	public static void main(String[] args) throws IOException{
		String url=getPropertyValue("application.url");
		System.out.println(url);
	}
	public static String getPropertyValue(String propertyName) throws IOException {
		// TODO Auto-generated method stub
		 config =  new Properties();
		 InputStream is= new FileInputStream("resources\\config\\RunConfig.properties");
		 config.load(is);
		 String propertyValue=config.getProperty(propertyName);
		 return propertyValue;
	}

}
