package com.evs.vtiger.framework.reusable_pages.marketing.leads;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.evs.vtiger.framework.generic_functions.UiUtil;
import com.evs.vtiger.framework.reusable_pages.BasePage;

public class CreateLeadPage extends BasePage{

	/**
	 * @param args
	 */
	@FindBy(name="firstname")
	public WebElement firstName_ED;

	@FindBy(name="lastname")
	public WebElement lastName_ED;

	@FindBy(name="company")
	public WebElement company_ED;

	@FindBy(xpath="//input[@class='crmbutton small save']")
	public WebElement save_BT;

	public  void saveLead() {
		// TODO Auto-generated method stub
		//////  
		UiUtil.input(firstName_ED, "CreateLead_FirstName_ED");	
		UiUtil.input(lastName_ED, "CreateLead_LastName_ED");	
		UiUtil.input(company_ED, "CreateLead_Company_ED");	
		UiUtil.click(save_BT);	

	}

	public  void cancelLead() {
		// TODO Auto-generated method stub

	}

}
