package com.evs.vtiger.framework.reusable_pages.marketing.leads;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.evs.vtiger.framework.generic_functions.UiUtil;
import com.evs.vtiger.framework.reusable_pages.BasePage;

public class LeadsLandingPage extends BasePage{

	/**
	 * @param args
	 */
	@FindBy(xpath="//img[@title='Create Lead...']")
	private WebElement createLead_BT;
	
	public  CreateLeadPage clickOnCreateLeadButton() {
		// TODO Auto-generated method stub
        UiUtil.click(createLead_BT);
        CreateLeadPage clp=PageFactory.initElements(UiUtil.driver, CreateLeadPage.class);
	    return clp;
	}

	public  void searchLeads() {
		// TODO Auto-generated method stub
//////  
	}
	
	
}
