package com.evs.vtiger.framework.reusable_pages.login;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.evs.vtiger.framework.generic_functions.PropUtil;
import com.evs.vtiger.framework.generic_functions.UiUtil;
import com.evs.vtiger.framework.reusable_pages.home.HomePage;

public class LoginPage {

	/**
	 * @param args
	 */
	@FindBy(name="user_name")
	private WebElement uname;
	
	@FindBy(xpath="//input[@name='user_password']")
	private WebElement password;
	
	@FindBy(name="Login")
	private WebElement loginBT;
		
	public  HomePage validLogin() throws IOException {
		// TODO Auto-generated method stub
		String userName=PropUtil.getPropertyValue("application.user.name");
		String userPassword=PropUtil.getPropertyValue("application.user.password");
		
		UiUtil.inputDirect(uname, userName);
		UiUtil.inputDirect(password, userPassword);
		UiUtil.click(loginBT);
		UiUtil.JS.click(loginBT);
		
		HomePage home=PageFactory.initElements(UiUtil.driver, HomePage.class);
		return home;
	}
	public  void invalidLogin() {
		// TODO Auto-generated method stub
////// 
		UiUtil.input(uname, "admin");
		UiUtil.input(password, "fxxxxg");
		UiUtil.click(loginBT);
		
	}	
	
}
