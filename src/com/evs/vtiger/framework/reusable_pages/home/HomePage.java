package com.evs.vtiger.framework.reusable_pages.home;
		
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.evs.vtiger.framework.generic_functions.UiUtil;
import com.evs.vtiger.framework.reusable_pages.marketing.leads.LeadsLandingPage;

public class HomePage {

	/**
	 * @param args
	 */
	@FindBy(linkText="Marketing")
	public WebElement weMarketing ;
	@FindBy(linkText="Leads")
	public WebElement weLeads;
	
	public LeadsLandingPage navigatesToLeads() {
		// TODO Auto-generated method stub
        UiUtil.Mouse.hover(weMarketing);
        UiUtil.click(weLeads);
       return PageFactory.initElements(UiUtil.driver, LeadsLandingPage.class);
	   
	}
	public void navigatesToAccounts() {
		// TODO Auto-generated method stub

	}
}
