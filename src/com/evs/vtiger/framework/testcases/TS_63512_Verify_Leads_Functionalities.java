package com.evs.vtiger.framework.testcases;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import com.evs.vtiger.framework.generic_functions.UiUtil;
import com.evs.vtiger.framework.generic_functions.dataUtil.DataManager;
import com.evs.vtiger.framework.reusable_pages.home.HomePage;
import com.evs.vtiger.framework.reusable_pages.login.LoginPage;
import com.evs.vtiger.framework.reusable_pages.marketing.leads.CreateLeadPage;
import com.evs.vtiger.framework.reusable_pages.marketing.leads.LeadsLandingPage;

public class TS_63512_Verify_Leads_Functionalities {

	/**
	 * @param args
	 * @throws IOException 
	 */
	
	public static void verify_Create_Lead() throws IOException {
		// TODO Auto-generated method stub
		  DataManager.getDataSource().getDataSourceType("TC_63514");
		LoginPage loginPage=UiUtil.openLoginPage();
		HomePage home=loginPage.validLogin();
		UiUtil.Validations.verifyInnerText(home.weMarketing, "V_MarketingText");
		LeadsLandingPage llp=home.navigatesToLeads();	
		CreateLeadPage clp=llp.clickOnCreateLeadButton();
		clp.saveLead();
		
		/////  validation	  
	}

	public static void verify_Delete_Lead() {
		// TODO Auto-generated method stub

	}

}
